package se331.backend.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.backend.rest.entity.Activity;


import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity, Long> {
    List<Activity> findAll();
    Activity findById(long id);
    Activity save(Activity activity);
}
