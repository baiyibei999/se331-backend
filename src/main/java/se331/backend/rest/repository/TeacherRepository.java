package se331.backend.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.backend.rest.entity.Teacher;

import java.util.List;

public interface TeacherRepository extends CrudRepository<Teacher, Long> {
    List<Teacher> findAll();
    Teacher findById(long id);
    Teacher save(Teacher teacher);
}
