package se331.backend.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.backend.rest.dto.ActivityDto;
import se331.backend.rest.dto.StudentDto;
import se331.backend.rest.dto.TeacherDto;
import se331.backend.rest.dto.UserDto;
import se331.backend.rest.entity.Activity;
import se331.backend.rest.entity.Admin;
import se331.backend.rest.entity.Student;
import se331.backend.rest.entity.Teacher;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper(MapperUtil.class);

    @Mappings({})
    TeacherDto getTeacherDto(Teacher teacher);
    @Mappings({})
    List<TeacherDto> getTeacherDto(List<Teacher> teachers);

    @Mappings({})
    StudentDto  getStudentDto(Student student);
    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);
    @Mappings({})
    List<Activity> getActivityDto(List<Activity> activities);

    @Mappings({ @Mapping(target = "authorities",source ="user.authorities" )})
    UserDto getUserDto(Student student);
    @Mappings({ @Mapping(target = "authorities",source ="user.authorities" )})
    UserDto getUserDto(Teacher teacher);
    @Mappings({ @Mapping(target = "authorities",source ="user.authorities" )})
    UserDto getUserDto(Admin admin);
}
