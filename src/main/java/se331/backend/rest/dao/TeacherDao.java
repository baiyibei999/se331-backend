package se331.backend.rest.dao;

import se331.backend.rest.entity.Student;
import se331.backend.rest.entity.Teacher;

import java.util.List;

public interface TeacherDao {
    List<Teacher> getAllTeachers();
    Teacher findById(long id);
    Teacher saveTeacher(Teacher teacher);
}
