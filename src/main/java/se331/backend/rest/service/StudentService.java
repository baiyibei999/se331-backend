package se331.backend.rest.service;

import se331.backend.rest.entity.Student;

import java.util.List;


public interface StudentService {
    List<Student> getAllStudents();
    Student findById(long id);
    Student saveStudent(Student student);
}
