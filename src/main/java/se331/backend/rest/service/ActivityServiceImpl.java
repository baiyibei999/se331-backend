package se331.backend.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.backend.rest.dao.ActivityDao;
import se331.backend.rest.entity.Activity;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    ActivityDao activityDao;
    @Override
    public List<Activity> getAllActivities() {
        return activityDao.getAllActivities();
    }

    @Override
    public Activity findById(long id) {
        return activityDao.findById(id);
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityDao.saveActivity(activity);
    }
}
