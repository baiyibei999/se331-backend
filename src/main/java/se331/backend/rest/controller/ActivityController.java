package se331.backend.rest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import se331.backend.rest.mapper.MapperUtil;
import se331.backend.rest.service.ActivityService;

@Controller
public class ActivityController {
    @Autowired
    ActivityService activityService;

    @Autowired

    @GetMapping("/activities")
    public ResponseEntity getActivities() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities()));
    }
}
